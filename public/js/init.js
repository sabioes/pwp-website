
/*-----------------------------------------------------------------------------------
/*
/* Init JS
/*
-----------------------------------------------------------------------------------*/

jQuery(document).ready(function ($) {

    //$('#buttonEmail').prop("disabled",true);


    /*----------------------------------------------------*/
    /* FitText Settings
    ------------------------------------------------------ */

    setTimeout(function () {
        $('h1.responsive-headline').fitText(1, { minFontSize: '40px', maxFontSize: '90px' });
    }, 100);


    /*----------------------------------------------------*/
    /* Smooth Scrolling
    ------------------------------------------------------ */

    $('.smoothscroll').on('click', function (e) {
        e.preventDefault();

        var target = this.hash,
            $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 800, 'swing', function () {
            window.location.hash = target;
        });
    });


    /*----------------------------------------------------*/
    /* Highlight the current section in the navigation bar
    ------------------------------------------------------*/

    var sections = $("section");
    var navigation_links = $("#nav-wrap a");

    sections.waypoint({

        handler: function (event, direction) {

            var active_section;

            active_section = $(this);
            if (direction === "up") active_section = active_section.prev();

            var active_link = $('#nav-wrap a[href="#' + active_section.attr("id") + '"]');

            navigation_links.parent().removeClass("current");
            active_link.parent().addClass("current");

        },
        offset: '35%'

    });


    /*----------------------------------------------------*/
    /*	Make sure that #header-background-image height is
    /* equal to the browser height.
    ------------------------------------------------------ */

    $('header').css({ 'height': $(window).height() });
    $(window).on('resize', function () {

        $('header').css({ 'height': $(window).height() });
        $('body').css({ 'width': $(window).width() })
    });


    /*----------------------------------------------------*/
    /*	Fade In/Out Primary Navigation
    ------------------------------------------------------*/

    $(window).on('scroll', function () {

        var h = $('header').height();
        var y = $(window).scrollTop();
        var nav = $('#nav-wrap');

        if ((y > h * .20) && (y < h) && ($(window).outerWidth() > 768)) {
            nav.fadeOut('fast');
        }
        else {
            if (y < h * .20) {
                nav.removeClass('opaque').fadeIn('fast');
            }
            else {
                nav.addClass('opaque').fadeIn('fast');
            }
        }

    });


    /*----------------------------------------------------*/
    /*	Modal Popup
    ------------------------------------------------------*/

    $('.item-wrap a').magnificPopup({

        type: 'inline',
        fixedContentPos: false,
        removalDelay: 200,
        showCloseBtn: false,
        mainClass: 'mfp-fade'

    });

    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });


    /*----------------------------------------------------*/
    /*	Flexslider
    /*----------------------------------------------------*/
    $('.flexslider').flexslider({
        namespace: "flex-",
        controlsContainer: ".flex-container",
        animation: 'slide',
        controlNav: true,
        directionNav: false,
        smoothHeight: true,
        slideshowSpeed: 7000,
        animationSpeed: 600,
        randomize: false,
    });

    /*----------------------------------------------------*/
    /*	contact form
    ------------------------------------------------------*/

    $('#contactForm button.submit').click(function () {
        var data = $("#contactForm").serializeArray()
        
        $('#contactForm').parsley().validate();
        
        if ($('#contactForm').parsley().isValid()) {
            $.ajax({
                method: 'POST',
                //url: 'http://192.168.178.65:8080/sendmail',
                url: 'https://pwp-server-pwp-server.1d35.starter-us-east-1.openshiftapps.com/sendmail',
                //url: 'http://serverysm.ml/sendmail',
                data: JSON.stringify({ data }),
                contentType: "application/json; charset=utf-8",
                success: function (data, textStatus, xhr) {
                    console.log(data)
                    // Message was sent
                    if (xhr.status == 200) {
                        $('form#contactForm button').text("Submited");
                        $('form#contactForm button').css('background', 'green');
                        setTimeout(function () {
                            $('form#contactForm button').text("Submit");
                            $('form#contactForm button').css('background', '#0D0D0D');
                        }, 2000);
                    }
                    // There was an error
                    else {
                        $('form#contactForm button').text("Error");
                        $('form#contactForm button').css('background', 'red');
                        setTimeout(function () {
                            $('form#contactForm button').text("Submit");
                            $('form#contactForm button').css('background', '#0D0D0D');
                        }, 2000);
                    }

                },
                error: function (status) {
                    console.log(status);
                    $('form#contactForm button').text("Error");
                    $('form#contactForm button').css('background', 'red');
                    setTimeout(function () {
                        $('form#contactForm button').text("Submit");
                        $('form#contactForm button').css('background', '#0D0D0D');
                    }, 2000);


                }


            });
            return false;
        }
        else {
            console.log('not valid');
        }
    });


});
/*----------------------------------------------------*/
/* Header animation
------------------------------------------------------ */






