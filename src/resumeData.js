let resumeData = {
  "imagebaseurl":"https://images.unsplash.com/",
  "name": "Carlos Silva",
  "role": "Frontend Developer",
  "emailid": "carlos_m_silva@outlook.com",
  "skypeid": "carlos_m_silva",
  "roleDescription": " I like dabbling in various parts of frontend development and like to learn about new technologies or simply play games in my free time.",
  "socialLinks":[
      {
        "name":"google-plus",
        "url":"https://www.xing.com/profile/Carlos_Silva108/cv?sc_o=mxb_p",
        "className":"fa fa-xing"
      },
      {
        "name":"linkedin",
        "url":"https://www.linkedin.com/in/carlosilvas/",
        "className":"fa fa-linkedin"
      },
      {
        "name":"skype",
        "url":"skype:carlos_m_silva?userinfo",
        "className":"fa fa-skype"
      },
      {
        "name":"whatsapp",
        "url":"https://api.whatsapp.com/send?phone=4917666997157",
        "className":"fa fa-whatsapp"
      }
    ],
  "aboutme":"My name is Carlos. I am Portuguese and I am twenty-four years old. I studied computer science and I have graduated in December of two thousand and seventeen. Six months ago I came to Germany. Since February I'm doing a German course. My native language is Portuguese and I speak a bit of German and English well. Professionally, in recent months I have been working with Java, Spark, and log4j. Also, I learned about DevOps Jenkins.",
  "address":"Bräunlingen 78199",
  "website":"https://www.youseeme.ml/",
  "education":[
    {
      "key":"2",
      "UniversityName":"University of Aveiro",
      "specialization":"Master's Degree in Computer Science",
      "MonthOfStarting":"September",
      "YearOfStarting":"2015",
      "MonthOfPassing":"December",
      "YearOfPassing":"2017",
      "Achievements":""
    },
    {
      "key":"1",
      "UniversityName":"ESTGA-University of Aveiro",
      "specialization":"Degree in Computer Science",
      "MonthOfStarting":"September",
      "YearOfStarting":"2012",
      "MonthOfPassing":"December",
      "YearOfPassing":"2015",
      "Achievements":""
    }
  ],
  "work":[
    {
      "key":"8",
      "CompanyName":"Freelance",
      "specialization":"Java, Spark-java, Pack4j",
      "MonthOfStarting":"December",
      "YearOfStarting":"2017",
      "MonthOfLeaving":"January",
      "YearOfLeaving":"2018",
      "Achievements":"Backend development"
    },
    {
      "key":"4",
      "CompanyName":"Freelance",
      "specialization":"Javascript, AngularJS, MySQL",
      "MonthOfStarting":"December",
      "YearOfStarting":"2016",
      "MonthOfLeaving":"September",
      "YearOfLeaving":"2016",
      "Achievements":"Frontend development"
    },
    {
      "key":"3",
      "CompanyName":"RM Consulting",
      "specialization":"C# programming",
      "MonthOfStarting":"May",
      "YearOfStarting":"2012",
      "MonthOfLeaving":"June",
      "YearOfLeaving":"2012",
      "Achievements":"System of key management"
    }
  ],
  "skillsDescription":"",
  "skills":[
    {
      "key":"5",
      "level": "75",
      "skillname":"Java(Spark Java, Pack4j, Java 8)"
    },
    {
      "key":"6",
      "level": "80",
      "skillname":"Javascript(AngularJS, React, jQuery)"
    },
    {
      "key":"7",
      "level": "75",
      "skillname":"Databases(MySQL, PostgreSQL, MSSQL)"
    },
    {
      "key":"8",
      "level": "65",
      "skillname":"DevOps(Jenkins,GitLab,Netlify)"
    },
    {
      "key":"9",
      "level": "65",
      "skillname":"Virtualization(Docker, Xen)"
    },
    {
      "key":"10",
      "level": "60",
      "skillname":"Python(Django)"
    },
    {
      "key":"11",
      "level": "100",
      "skillname":"Development Tools(Intellij, Visual Studio Code)"
    },
  ],
  "portfolio":[
    {
      "name":"Project X",
      "description":"Under developing",
      "imgurl":"photo-1499750310107-5fef28a66643?ixlib=rb-0.3.5&s=c0ac25c3cd56ced9821b924fed4c3c43&auto=format&fit=crop&w=1950&q=80"
    },
    {
      "name":"Project Y",
      "description":"Under developing",
      "imgurl":"photo-1489875347897-49f64b51c1f8?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=7ca95b1290dfb28db0ee368546e55487&auto=format&fit=crop&w=1950&q=80"
    },
    {
      "name":"Project Z",
      "description":"Under developing",
      "imgurl":"photo-1492551557933-34265f7af79e?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5f9e7b01025c2911f850db34fae63a8a&auto=format&fit=crop&w=1950&q=80"
    },
    {
      "name":"Project U",
      "description":"Under developing",
      "imgurl":"photo-1519893578517-3252b77e8fdf?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=360a6f43b0507da46c0a9259c42fa265&auto=format&fit=crop&w=1349&q=80"
    }
  ],
  "testimonials":[
    {
      "key":"1",
      "description":"sample testimonial",
      "name":"guy"
    },
    {
      "key":"2",
      "description":"sample testimonial",
      "name":"guy"
    }
  ]
}

export default resumeData
