import React from 'react';

class BarSkill extends React.Component {
  constructor() {
    super();
    this.state = { text: '' }
  }
  //set the text
  onMouseover(e) {
    this.setState({ text: this.props.item.level +'%'})
  }
  //clear the text
  onMouseout(e) {
    this.setState({ text: '' })
  }
  render() {
    const { text } = this.state;
    let data = this.props.item;
    return (
      <div>
        <div className={`bar-expand ${data.skillname.toLowerCase()}`}
          style={{width: this.props.item.level +'%'}}
          //onMouseEnter={this.onMouseover.bind(this)}
          //onMouseLeave={this.onMouseout.bind(this)}
          >
          <h2 className='text-right'>{text}</h2>
        </div>
        <em>{data.skillname}</em>
      </div>
    );
  }
}

export default BarSkill;