import React, { Component } from 'react';
export default class ContactUs extends Component {
  render() {
    //let resumeData = this.props.resumeData;
    return (
      <section id="contact">
        <div className="row contactus">
          <div className="three columns header-col">
            <h1><span>Contact Me</span></h1>
          </div>
          <div style={{ marginTop: '10px' }} className="nine columns footer-widgets">
            <div className="text"><h1>  </h1></div>
            <form style={{ marginTop: '30px' }} action="" method="post" id="contactForm" name="contactForm">
              <fieldset>
                <div>
                  <label htmlFor="contactName">Name <span className="required">*</span></label>
                  <input type="text" id="contactName" name="contactName" required/>
                </div>
                <div>
                  <label htmlFor="contactEmail">Email <span className="required">*</span></label>
                  <input type="email" name="contactEmail" required/>
                </div>

                <div>
                  <label htmlFor="contactSubject">Subject</label>
                  <input type="text" id="contactSubject" name="contactSubject" />
                </div>

                <div>
                  <label htmlFor="contactMessage">Message <span className="required">*</span></label>
                  <textarea rows="15" id="contactMessage" name="contactMessage" required></textarea>
                </div>

                <div style={{ display: 'flex' }}>
                  <button id="buttonEmail" className="submit" value="validate">Submit</button>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </section>
    );
  }
}
